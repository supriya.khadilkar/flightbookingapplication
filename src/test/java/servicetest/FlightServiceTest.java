package servicetest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.stories.flightbooking.exceptions.FlightsNotFoundException;
import org.stories.flightbooking.exceptions.InvalidSearchCriteriaException;
import org.stories.flightbooking.exceptions.RouteNotFoundException;
import org.stories.flightbooking.model.*;
import org.stories.flightbooking.repository.FlightRepository;
import org.stories.flightbooking.repository.RouteRepository;
import org.stories.flightbooking.sevice.FlightService;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FlightServiceTest {

    @Mock
    RouteRepository routeRepository;

    @Mock
    FlightRepository flightRepository;

    @InjectMocks
    FlightService flightService;


//    List<FlightCoverage> fcs = new ArrayList<>();


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /*Story -1 */

    @Test(expected = InvalidSearchCriteriaException.class)
    public void shouldThrowInvalidSearchCriteriaExceptionGivenEmptySource() throws InvalidSearchCriteriaException, RouteNotFoundException, FlightsNotFoundException {
        SearchCriteria sc = new SearchCriteria();
        sc.setSource("");
        flightService.searchFlightsForGivenCriteria(sc);
    }

    @Test(expected = InvalidSearchCriteriaException.class)
    public void shouldThrowInvalidSearchCriteriaExceptionGivenEmptyDestination() throws InvalidSearchCriteriaException, RouteNotFoundException, FlightsNotFoundException {
        SearchCriteria sc = new SearchCriteria();
        sc.setSource("HYD");
        sc.setDestination("");
       flightService.searchFlightsForGivenCriteria(sc);
    }

    @Test(expected = RouteNotFoundException.class)
    public void shouldThrowRouteNotFoundExceptionGivenSourceDestination() throws InvalidSearchCriteriaException, RouteNotFoundException, FlightsNotFoundException {
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);
        SearchCriteria sc = new SearchCriteria();
        sc.setSource("HYD");
        sc.setDestination("DELI");
        flightService.searchFlightsForGivenCriteria(sc);
    }

    @Test(expected = FlightsNotFoundException.class)
    public void shouldReturnEmptyFlightListForGivenRoute() throws InvalidSearchCriteriaException, RouteNotFoundException, FlightsNotFoundException {
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);

        List<Flight> flights = findAllFlights();
        when(flightRepository.findAllFlights()).thenReturn(flights);

        SearchCriteria sc = new SearchCriteria();
        sc.setSource("MUM");
        sc.setDestination("CAL");
        flightService.searchFlightsForGivenCriteria(sc);

    }

    @Test
    public void shouldReturnAllFlightsForGivenSourceAndDestination() throws InvalidSearchCriteriaException, RouteNotFoundException, FlightsNotFoundException {
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);

        List<Flight> flights = findAllFlights();
        when(flightRepository.findAllFlights()).thenReturn(flights);

        List<FlightClasses> flightClasses = findAllClasses();
        when(flightRepository.findAllClasses()).thenReturn(flightClasses);

        List<Flight> expected = getExpectedFlightList();
        SearchCriteria searchCri = new SearchCriteria();
        searchCri.setDestination("DEL");
        searchCri.setSource("HYD");

        List<FlightSearchResult> result = flightService.searchFlightsForGivenCriteria(searchCri);
        Assert.assertEquals(expected.size(), result.size());
    }

    @Test()
    public void shouldReturnOnlyFlightsWhichMatchesWithGivenTravelClass() throws RouteNotFoundException, FlightsNotFoundException, InvalidSearchCriteriaException {
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);

        List<Flight> flights = findAllFlights();
        when(flightRepository.findAllFlights()).thenReturn(flights);

        List<FlightClasses> flightClasses = findAllClasses();
        when(flightRepository.findAllClasses()).thenReturn(flightClasses);

        List<Flight> expectedFlts = new ArrayList<>();

        SearchCriteria searchCri = new SearchCriteria();
        searchCri.setDestination("DEL");
        searchCri.setSource("HYD");
        searchCri.setTravelClass("FC");

        Flight flight1 = new Flight("F001", "200LR(77L)", "Boeing 777", 2L);
        expectedFlts.add(flight1);
        System.out.println("size:" + expectedFlts.size());

        List<FlightSearchResult> result = flightService.searchFlightsForGivenCriteria(searchCri);
        System.out.println("Result Size: "+ result.size());
        // Assert.assertEquals(expectedFlts.size(), result.size());
        Assert.assertEquals(expectedFlts.size(), result.size());
    }



    @Test()
    public void shouldReturnOnlyFlightsWhichMatchesWithDefaultTravelClass() throws RouteNotFoundException, FlightsNotFoundException, InvalidSearchCriteriaException {
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);

        List<Flight> flights = findAllFlights();
        when(flightRepository.findAllFlights()).thenReturn(flights);

        List<FlightClasses> flightClasses = findAllClasses();
        when(flightRepository.findAllClasses()).thenReturn(flightClasses);

        List<Flight> expectedFlts = getExpectedFlightList();

        SearchCriteria searchCri = new SearchCriteria();
        searchCri.setDestination("DEL");
        searchCri.setSource("HYD");
        //searchCri.setTravelClass("FC");


        List<FlightSearchResult> result = flightService.searchFlightsForGivenCriteria(searchCri);
        System.out.println("Result Size: "+ result.size());
        // Assert.assertEquals(expectedFlts.size(), result.size());
        Assert.assertEquals(expectedFlts.size(), result.size());
    }

    @Test
    public void shouldReturnTotalFareSameAsBasePriceIfAllEconomySeatsAreAvailableOnSomeDate() throws RouteNotFoundException, FlightsNotFoundException, InvalidSearchCriteriaException {
        LocalDate now = LocalDate.now();
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);

        List<Flight> flights = findAllFlights();
        when(flightRepository.findAllFlights()).thenReturn(flights);

        List<FlightClasses> flightClasses = findAllClasses();
        when(flightRepository.findAllClasses()).thenReturn(flightClasses);

        List<FlightSeatsStatus> seatsStatuses = findAllFlightSeatsStatus();
        when(flightRepository.findAllFlightSeatsStatus()).thenReturn(seatsStatuses);

        SearchCriteria sc = new SearchCriteria();
        sc.setTravelClass("EC");
        sc.setDepartureDate(now.plusDays(1));
        sc.setSource("HYD");
        sc.setDestination("DEL");
        sc.setNoOfPax(2);

        List<FlightSearchResult> results = flightService.searchFlightsForGivenCriteria(sc);
        Double expectedFare = 12000.0;
        Optional<FlightSearchResult> optFsr = results.stream().filter(res -> res.getFlightId().equals("F001")).findFirst();
        Assert.assertTrue(optFsr.isPresent());
        Assert.assertEquals(expectedFare, optFsr.get().getTotalFare());
    }



    @Test
    public void shouldReturnTotalFareForEconomySeatAsBasepriceHikedAsPerPremuimRatioOnSomeDate() throws RouteNotFoundException, FlightsNotFoundException, InvalidSearchCriteriaException {
        LocalDate now = LocalDate.now();
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);

        List<Flight> flights = findAllFlights();
        when(flightRepository.findAllFlights()).thenReturn(flights);

        List<FlightClasses> flightClasses = findAllClasses();
        when(flightRepository.findAllClasses()).thenReturn(flightClasses);

        List<FlightSeatsStatus> seatsStatuses = findAllFlightSeatsStatus();
        when(flightRepository.findAllFlightSeatsStatus()).thenReturn(seatsStatuses);

        SearchCriteria sc = new SearchCriteria();
        sc.setTravelClass("EC");
        sc.setDepartureDate(now.plusDays(5));
        sc.setSource("HYD");
        sc.setDestination("DEL");
        sc.setNoOfPax(2);


        List<FlightSearchResult> results = flightService.searchFlightsForGivenCriteria(sc);
        //results.stream().forEach(res -> System.out.println(res.getTotalFare()));
        Double expectedFare = 15600.0;
        Optional<FlightSearchResult> optFsr = results.stream().filter(res -> res.getFlightId().equals("F001")).findFirst();
        Assert.assertTrue(optFsr.isPresent());
        Assert.assertEquals(expectedFare, optFsr.get().getTotalFare());
    }

    /**
     *
     * @throws RouteNotFoundException
     * @throws FlightsNotFoundException
     * @throws InvalidSearchCriteriaException
     * Before running this test, set departuredate matches with MONDAY or FRIDAY or SUNDAY
     */
    @Test
    public void shouldReturnTotalFareForBusiessSeatAsBasepriceHikedAsPerPremuimRatioOnMONDAY_FRIDAY_SUNDAY() throws RouteNotFoundException, FlightsNotFoundException, InvalidSearchCriteriaException {
        /*Business-class will be charged 40% extra of the base price if the travel is on Monday, Friday or Sunday.
         * Cosidered : Total Seats are 35 and Base price is 13000.0 for flight id F001*/

        LocalDate now = LocalDate.now();
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);

        List<Flight> flights = findAllFlights();
        when(flightRepository.findAllFlights()).thenReturn(flights);

        List<FlightClasses> flightClasses = findAllClasses();
        when(flightRepository.findAllClasses()).thenReturn(flightClasses);

        List<FlightSeatsStatus> seatsStatuses = findAllFlightSeatsStatus();
        when(flightRepository.findAllFlightSeatsStatus()).thenReturn(seatsStatuses);

        /*Setting up search criteria*/

        SearchCriteria sc = new SearchCriteria();
        sc.setTravelClass("BC");
        sc.setDepartureDate(now.with(TemporalAdjusters.next(DayOfWeek.FRIDAY)));
        sc.setSource("HYD");
        sc.setDestination("DEL");
        sc.setNoOfPax(2);

        /*getting results*/
        List<FlightSearchResult> results = flightService.searchFlightsForGivenCriteria(sc);
        //results.stream().forEach(res -> System.out.println(res.getTotalFare()));

        Double expectedFare = 36400.0;

        Optional<FlightSearchResult> optFsr = results.stream().filter(res -> res.getFlightId().equals("F001")).findFirst();
        Assert.assertTrue(optFsr.isPresent());
        Assert.assertEquals(expectedFare, optFsr.get().getTotalFare());
    }

    @Test
    public void shouldReturnTotalFareForBusiessSeatAsBasepriceWhenDayisOtherThanMONDAY_FRIDAY_SUNDAY() throws RouteNotFoundException, FlightsNotFoundException, InvalidSearchCriteriaException {
        /*Business-class will be charged 40% extra of the base price if the travel is on Monday, Friday or Sunday.
         * Cosidered : Total Seats are 35 and Base price is 13000.0 for flight id F001*/

        LocalDate now = LocalDate.now();
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);

        List<Flight> flights = findAllFlights();
        when(flightRepository.findAllFlights()).thenReturn(flights);

        List<FlightClasses> flightClasses = findAllClasses();
        when(flightRepository.findAllClasses()).thenReturn(flightClasses);

        List<FlightSeatsStatus> seatsStatuses = findAllFlightSeatsStatus();
        when(flightRepository.findAllFlightSeatsStatus()).thenReturn(seatsStatuses);

        /*Setting up search criteria*/

        SearchCriteria sc = new SearchCriteria();
        sc.setTravelClass("BC");
        sc.setDepartureDate(now.with(TemporalAdjusters.next(DayOfWeek.THURSDAY)));
        sc.setSource("HYD");
        sc.setDestination("DEL");
        sc.setNoOfPax(2);

        /*getting results*/
        List<FlightSearchResult> results = flightService.searchFlightsForGivenCriteria(sc);
        //results.stream().forEach(res -> System.out.println(res.getTotalFare()));

        Double expectedFare = 26000.0;

        Optional<FlightSearchResult> optFsr = results.stream().filter(res -> res.getFlightId().equals("F001")).findFirst();
        Assert.assertTrue(optFsr.isPresent());
        Assert.assertEquals(expectedFare, optFsr.get().getTotalFare());
    }


    @Test
    public void shouldReturnTotalFareForFirstClassSeatsTenPercentHikeOfBasePriceEveryDay() throws RouteNotFoundException, FlightsNotFoundException, InvalidSearchCriteriaException {
        /*First class will be opened 10 days before the departure date and everyday the extra charge will be 10% more. for flight no "F001"*/
        /*Considered flight's  departure date 4 days from today so 0.4 of baseprice is added to baseprice
         * Baseprice is 20000*/

        LocalDate now = LocalDate.now();
        List<Route> routes = findAllRoutes();
        when(routeRepository.findAllRoutes()).thenReturn(routes);

        List<Flight> flights = findAllFlights();
        when(flightRepository.findAllFlights()).thenReturn(flights);

        List<FlightClasses> flightClasses = findAllClasses();
        when(flightRepository.findAllClasses()).thenReturn(flightClasses);

        List<FlightSeatsStatus> seatsStatuses = findAllFlightSeatsStatus();
        when(flightRepository.findAllFlightSeatsStatus()).thenReturn(seatsStatuses);

        /*Setting up search criteria*/
        SearchCriteria sc = new SearchCriteria();
        sc.setTravelClass("FC");
        sc.setDepartureDate(now.plusDays(1));
        sc.setSource("HYD");
        sc.setDestination("DEL");
        sc.setNoOfPax(1);

        /*getting results*/
        List<FlightSearchResult> results = flightService.searchFlightsForGivenCriteria(sc);
        //results.stream().forEach(res -> System.out.println(res.getTotalFare()));

        Double expectedFare = 38000.0;

        Optional<FlightSearchResult> optFsr = results.stream().filter(res -> res.getFlightId().equals("F001")).findFirst();
        Assert.assertTrue(optFsr.isPresent());
        Assert.assertEquals(expectedFare, optFsr.get().getTotalFare());

    }


    /*** Dummy Data creation***/
    public List<Route> findAllRoutes()
    {
        List<Route> routes = new ArrayList<>();
        Route route1 = new Route(1L, "HYD", "NGP");
        routes.add(route1);
        Route route2 = new Route(2L, "HYD", "DEL");
        routes.add(route2);
        Route route3 = new Route(3L, "HYD", "PNQ");
        routes.add(route3);
        Route route7 = new Route(6L, "MUM", "CAL");
        routes.add(route7);
        return routes;
    }

    public List<Flight> findAllFlights() {

        List<Flight> flights = new ArrayList<>();
        Flight flight1 = new Flight("F001", "200LR(77L)", "Boeing 777", 2l);
        flights.add(flight1);
        Flight flight2 = new Flight("F002", "200LR(77L)", "Boeing 777", 3l);
        flights.add(flight2);
        Flight flight6 = new Flight("F008", "Airbus A319 V2", "Airbus A319", 2l);
        flights.add(flight6);
        return  flights;
    }

    public List<Flight> getExpectedFlightList()
    {
        List<Flight> flights = new ArrayList<>();
        Flight flight1 = new Flight("F001", "200LR(77L)", "Boeing 777", 2l);
        flights.add(flight1);
        Flight flight6 = new Flight("F008", "Airbus A319 V2", "Airbus A319", 2l);
        flights.add(flight6);
        return  flights;
    }

    public List<FlightClasses> findAllClasses() {
        List<FlightClasses> flightClasses = new ArrayList<>();
        FlightClasses fcs1 = new FlightClasses("FC", "First Class", 8d, 20000.0, "F001");
        flightClasses.add(fcs1);
        FlightClasses fcs2 = new FlightClasses("BC", "Business Class", 35d, 13000.0, "F001");
        flightClasses.add(fcs2);
        FlightClasses fcs3 = new FlightClasses("EC", "Economy", 195d, 6000.0, "F001");
        flightClasses.add(fcs3);
        FlightClasses fc13 = new FlightClasses("EC", "Economy", 144d, 6000.0, "F008");
        flightClasses.add(fc13);
        return flightClasses;
    }

    public List<FlightSeatsStatus> findAllFlightSeatsStatus()
    {
        LocalDate now = LocalDate.now();
        List<FlightSeatsStatus> flightSeatsStatuses = new ArrayList<>();

        FlightSeatsStatus status1 = new FlightSeatsStatus("FC", "F001", now.plusDays(4), 3d);
        flightSeatsStatuses.add(status1);

        FlightSeatsStatus status2 = new FlightSeatsStatus("BC", "F001", now.with(TemporalAdjusters.next(DayOfWeek.FRIDAY)), 20d);
        flightSeatsStatuses.add(status2);

        FlightSeatsStatus status3 = new FlightSeatsStatus("EC", "F001", now.plusDays(1), 0d);
        flightSeatsStatuses.add(status3);

        FlightSeatsStatus status6 = new FlightSeatsStatus("EC", "F001", now.plusDays(5), 100d);
        flightSeatsStatuses.add(status6);

        FlightSeatsStatus status4 = new FlightSeatsStatus("EC", "F008", now.plusDays(1), 70d);
        flightSeatsStatuses.add(status4);



        FlightSeatsStatus status5 = new FlightSeatsStatus("BC", "F001", now.with(TemporalAdjusters.next(DayOfWeek.THURSDAY)), 10d);
        flightSeatsStatuses.add(status5);

        FlightSeatsStatus status7 = new FlightSeatsStatus("FC", "F001", now.plusDays(1), 4d);
        flightSeatsStatuses.add(status7);

        FlightSeatsStatus status8 = new FlightSeatsStatus("FC", "F001", now.plusDays(2), 4d);
        flightSeatsStatuses.add(status8);
        FlightSeatsStatus status9 = new FlightSeatsStatus("BC", "F001", now.plusDays(2), 5d);
        flightSeatsStatuses.add(status9);
        FlightSeatsStatus status10 = new FlightSeatsStatus("EC", "F001", now.plusDays(2), 45d);
        flightSeatsStatuses.add(status10);

        FlightSeatsStatus status11 = new FlightSeatsStatus("EC", "F008", now.plusDays(2), 75d);
        flightSeatsStatuses.add(status11);
        FlightSeatsStatus status12 = new FlightSeatsStatus("EC", "F008", now.plusDays(3), 75d);
        flightSeatsStatuses.add(status12);

        return flightSeatsStatuses;
    }
}
