package repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.stories.flightbooking.Application;
import org.stories.flightbooking.repository.RouteRepository;


import java.util.Optional;
/*
//@RunWith(SpringRunner.class)
@SpringBootTest(classes =  Application.class)
//@JdbcTest
@ComponentScan
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(Application.class)
public class RouteRepositoryTest
{
    @Autowired
    RouteRepository routeRepository;

    @Test
    public void shouldReturnOptionalRouteIdForGiveCorrectSourceAndDestination()
    {
        //Integer rtId = Integer.valueOf(6);
       // Optional<Integer> expectedRtId = Optional.of(rtId);
        Optional<Integer> resultOptRtID = routeRepository.findRoutIdForGivenSourceAndDestination("HYD", "NGP");
        Assert.assertEquals(true, resultOptRtID.isPresent());
    }
}*/
