package org.stories.flightbooking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.stories.flightbooking.exceptions.FlightsNotFoundException;
import org.stories.flightbooking.exceptions.InvalidSearchCriteriaException;
import org.stories.flightbooking.exceptions.RouteNotFoundException;
import org.stories.flightbooking.model.FlightSearchResult;
import org.stories.flightbooking.model.SearchCriteria;
import org.stories.flightbooking.sevice.FlightService;

import java.io.IOException;
import java.util.List;

@RestController
public class FlightController
{

    @Autowired
    FlightService flightService;


    @GetMapping(value = "/searchFlights")
    public @ResponseBody List<FlightSearchResult> searchFlights(@RequestParam(name = "criteria") String criteria)
                            throws RouteNotFoundException, FlightsNotFoundException, InvalidSearchCriteriaException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        mapper.registerModule(javaTimeModule);
        SearchCriteria sc = mapper.readValue(criteria, SearchCriteria.class);
        return  flightService.searchFlightsForGivenCriteria(sc);
    }


}
