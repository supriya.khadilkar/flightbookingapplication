package org.stories.flightbooking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class HelloController
{

    @GetMapping(value = "/flightsPage")
    public String getFlightSearchPage()
    {
        return "searchFlight";
    }

    @RequestMapping("/hello")
    public String index() {
        return "Greetings from Spring Boot!";
    }
}
