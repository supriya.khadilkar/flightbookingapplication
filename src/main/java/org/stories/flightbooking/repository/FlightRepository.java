package org.stories.flightbooking.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.stories.flightbooking.model.Flight;
import org.stories.flightbooking.model.FlightClasses;
import org.stories.flightbooking.model.FlightSeatsStatus;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FlightRepository
{

    @Autowired
    RouteRepository routeRepository;


    public List<Flight> findAllFlights()
    {

        List<Flight> flights = new ArrayList<>();
        Flight flight1 = new Flight("F001", "200LR(77L)", "Boeing 777", 2l);
        flights.add(flight1);
        Flight flight2 = new Flight("F002", "200LR(77L)", "Boeing 777", 3l);
        flights.add(flight2);
        Flight flight3 = new Flight("F005", "200LR(77L)", "Boeing 777", 10l);
        flights.add(flight3);
        Flight flight4 = new Flight("F006", "Airbus A319 V2", "Airbus A319", 5l);
        flights.add(flight4);
        Flight flight5 = new Flight("F007", "Airbus A321", "Airbus A319", 8l);
        flights.add(flight5);
        Flight flight6 = new Flight("F008", "Airbus A319 V2", "Airbus A319", 2l);
        flights.add(flight6);
        return  flights;

    }

    public List<FlightClasses> findAllClasses()
    {
        List<FlightClasses>  flightClasses = new ArrayList<>();
        FlightClasses fcs1 = new FlightClasses("FC", "First Class", 8d, 20000.0, "F001");
        flightClasses.add(fcs1);
        FlightClasses fcs2 = new FlightClasses("BC", "Business Class", 35d, 13000.0, "F001");
        flightClasses.add(fcs2);
        FlightClasses fcs3 = new FlightClasses("EC", "Economy", 195d, 6000.0, "F001");
        flightClasses.add(fcs3);

        FlightClasses fcs4 = new FlightClasses("FC", "First Class", 8d, 10000.0, "F002");
        flightClasses.add(fcs4);
        FlightClasses fcs5 = new FlightClasses("BC", "Business Class", 35d, 14000.0, "F002");
        flightClasses.add(fcs5);
        FlightClasses fcs6 = new FlightClasses("EC", "Economy", 195d, 6000.0, "F002");
        flightClasses.add(fcs6);

        FlightClasses fc7 = new FlightClasses("FC", "First Class", 8d, 15000.0 , "F005");
        flightClasses.add(fc7);
        FlightClasses fc8 = new FlightClasses("BC", "Business Class", 35d, 8000.0, "F005");
        flightClasses.add(fc8);
        FlightClasses fc9 = new FlightClasses("EC", "Economy", 195d, 6000.0, "F005");
        flightClasses.add(fc9);

        FlightClasses fc10 = new FlightClasses("EC", "Economy", 144d, 6000.0, "F006");
        flightClasses.add(fc9);

        FlightClasses fc11 = new FlightClasses("BC", "Business Class", 35d, 13000.0, "F007");
        flightClasses.add(fc8);
        FlightClasses fc12 = new FlightClasses("EC", "Economy", 195d, 6000.0, "F007");
        flightClasses.add(fc9);

        FlightClasses fc13 = new FlightClasses("EC", "Economy", 144d, 6000.0, "F008");
        flightClasses.add(fc13);
        return  flightClasses;
    }

    public List<FlightSeatsStatus> findAllFlightSeatsStatus()
    {
        LocalDate now = LocalDate.now();
        List<FlightSeatsStatus> flightSeatsStatuses = new ArrayList<>();
        FlightSeatsStatus status1 = new FlightSeatsStatus("FC", "F001", now.plusDays(4), 3d);
        flightSeatsStatuses.add(status1);

        FlightSeatsStatus status2 = new FlightSeatsStatus("BC", "F001", now.with(TemporalAdjusters.next(DayOfWeek.FRIDAY)), 20d);
        flightSeatsStatuses.add(status2);

        FlightSeatsStatus status3 = new FlightSeatsStatus("EC", "F001", now.plusDays(1), 0d);
        flightSeatsStatuses.add(status3);

        FlightSeatsStatus status6 = new FlightSeatsStatus("EC", "F001", now.plusDays(5), 100d);
        flightSeatsStatuses.add(status6);

        FlightSeatsStatus status4 = new FlightSeatsStatus("EC", "F008", now.plusDays(1), 70d);
        flightSeatsStatuses.add(status4);



        FlightSeatsStatus status5 = new FlightSeatsStatus("BC", "F001", now.with(TemporalAdjusters.next(DayOfWeek.THURSDAY)), 10d);
        flightSeatsStatuses.add(status5);

        FlightSeatsStatus status7 = new FlightSeatsStatus("FC", "F001", now.plusDays(1), 4d);
        flightSeatsStatuses.add(status7);

        FlightSeatsStatus status8 = new FlightSeatsStatus("FC", "F001", now.plusDays(2), 4d);
        flightSeatsStatuses.add(status8);

        FlightSeatsStatus status10 = new FlightSeatsStatus("EC", "F001", now.plusDays(2), 45d);
        flightSeatsStatuses.add(status10);

        FlightSeatsStatus status11 = new FlightSeatsStatus("EC", "F008", now.plusDays(2), 75d);
        flightSeatsStatuses.add(status11);
        FlightSeatsStatus status12 = new FlightSeatsStatus("EC", "F008", now.plusDays(3), 75d);
        flightSeatsStatuses.add(status12);

        FlightSeatsStatus status15 = new FlightSeatsStatus("FC", "F001", now.plusDays(0), 2d);
        flightSeatsStatuses.add(status15);

        FlightSeatsStatus status16 = new FlightSeatsStatus("FC", "F001", now.plusDays(3), 2d);
        flightSeatsStatuses.add(status16);
        return flightSeatsStatuses;
    }





}
