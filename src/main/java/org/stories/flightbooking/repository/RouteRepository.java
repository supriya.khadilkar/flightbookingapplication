package org.stories.flightbooking.repository;

import org.springframework.stereotype.Repository;
import org.stories.flightbooking.model.Route;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RouteRepository
{

   public List<Route> findAllRoutes()
   {
       List<Route> routes = new ArrayList<>();
       Route route1 = new Route(1l,"HYD", "NGP");
       routes.add(route1);
       Route route2 = new Route(2l,"HYD", "DEL");
       routes.add(route2);
       Route route3 = new Route(3l,"HYD", "PNQ");
       routes.add(route3);
       Route route4 = new Route(4l,"PNQ", "HYD");
       routes.add(route4);
       Route route5 = new Route(5l,"DEL", "HYD");
       routes.add(route5);
       Route route6 = new Route(6l, "DEL", "PNQ");
       routes.add(route6);
       Route route7 = new Route(7l, "MUM", "CAL");
       routes.add(route7);
       return  routes;
   }


}
