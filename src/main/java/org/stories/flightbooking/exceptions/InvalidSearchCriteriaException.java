package org.stories.flightbooking.exceptions;

public class InvalidSearchCriteriaException extends Exception
{
    private String message;

    public InvalidSearchCriteriaException(String message)
    {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
