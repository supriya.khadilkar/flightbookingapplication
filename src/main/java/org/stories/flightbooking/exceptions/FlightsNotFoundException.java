package org.stories.flightbooking.exceptions;

public class FlightsNotFoundException extends Exception
{
    private String message;

    public FlightsNotFoundException(String message)
    {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
