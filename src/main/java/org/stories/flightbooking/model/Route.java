package org.stories.flightbooking.model;

public class Route
{

    private Long routeId;

    private String source;

    private String destination;

    public Route(Long routeId, String source, String destination)
    {
        this.routeId = routeId;
        this.source = source;
        this.destination = destination;
    }

    public Long getRouteId()
    {
        return routeId;
    }

    public void setRouteId(Long routeId)
    {
        this.routeId = routeId;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
