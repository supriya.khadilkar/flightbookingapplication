package org.stories.flightbooking.model;


import java.time.LocalDate;


public class FlightSeatsStatus
{
    private String classCode;
    private String flightId;
    private LocalDate departureDate;
    private Double bookedSeats;


    public FlightSeatsStatus(String classCode, String flightId, LocalDate departureDate, Double bookedSeats)
    {
        this.classCode = classCode;
        this.flightId = flightId;
        this.departureDate = departureDate;
        this.bookedSeats = bookedSeats;
    }

    public Double getBookedSeats() {
        return bookedSeats;
    }

    public void setBookedSeats(Double bookedSeats) {
        this.bookedSeats = bookedSeats;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }
}
