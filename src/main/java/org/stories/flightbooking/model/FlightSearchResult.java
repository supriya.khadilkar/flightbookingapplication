package org.stories.flightbooking.model;

public class FlightSearchResult
{
    private String flightId;
    private String flightName;
    private String travelClass;
    private Double totalFare;
    private Integer noOfPax;


    public FlightSearchResult(String flightId, String flightName, String travelClass, Double totalFare, Integer noOfPax)
    {
        this.flightId = flightId;
        this.flightName = flightName;
        this.travelClass = travelClass;
        this.totalFare = totalFare;
        this.noOfPax = noOfPax;
    }

    public FlightSearchResult()
    {
    }


    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public Double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(Double totalFare) {
        this.totalFare = totalFare;
    }

    public Integer getNoOfPax() {
        return noOfPax;
    }

    public void setNoOfPax(Integer noOfPax) {
        this.noOfPax = noOfPax;
    }
}
