package org.stories.flightbooking.model;

import java.io.Serializable;


public class Flight implements Serializable
{
    private String fightId;

    private String modelName;

    private String flightName;
    
    private Long fk_routeId;


    public Flight(String fightId, String modelName, String flightName, Long fk_routeId) {
        this.fightId = fightId;
        this.modelName = modelName;
        this.flightName = flightName;
        this.fk_routeId = fk_routeId;
    }

    public String getFightId() {
        return fightId;
    }

    public void setFightId(String fightId) {
        this.fightId = fightId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public Long getFk_routeId() {
        return fk_routeId;
    }

    public void setFk_routeId(Long fk_routeId) {
        this.fk_routeId = fk_routeId;
    }
}
