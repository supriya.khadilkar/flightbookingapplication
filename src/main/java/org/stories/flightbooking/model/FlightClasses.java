package org.stories.flightbooking.model;


public class FlightClasses
{
   // private Integer fltClsId;
    private String classCode;
    private String classTypeName;
    private Double totalSeats;
    private Double basePrice;
    private String flightId;


    public FlightClasses(String classCode, String classTypeName, Double totalSeats, Double basePrice, String flightId) {
        this.classCode = classCode;
        this.classTypeName = classTypeName;
        this.totalSeats = totalSeats;
        this.basePrice = basePrice;
        this.flightId = flightId;
    }


    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public Double getTotalSeats() {
        return totalSeats;
    }

    public void setTotalSeats(Double totalSeats) {
        this.totalSeats = totalSeats;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }
}
