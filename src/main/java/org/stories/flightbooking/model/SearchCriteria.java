package org.stories.flightbooking.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

@JsonDeserialize(as = SearchCriteria.class)
public class SearchCriteria implements Serializable
{
    private String source;
    private String destination;
    //2019-08-26 yyyy-MM-dd
    @JsonFormat(pattern = "yyyy-MM-dd")
    // Allows dd/MM/yyyy date to be passed into GET request in JSON
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate departureDate;
    private Integer noOfPax;
    private String travelClass;

    public SearchCriteria()
    {

    }


    public SearchCriteria(String source, String destination, LocalDate departureDate, Integer noOfPax, String travelClass)
    {

        this.source = source;
        this.destination = destination;
        this.departureDate = departureDate;
        this.noOfPax = noOfPax;
        this.travelClass = travelClass;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public Integer getNoOfPax() {
        return noOfPax;
    }

    public void setNoOfPax(Integer noOfPax) {
        this.noOfPax = noOfPax;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }
}
