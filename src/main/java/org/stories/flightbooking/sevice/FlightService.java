package org.stories.flightbooking.sevice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stories.flightbooking.exceptions.FlightsNotFoundException;
import org.stories.flightbooking.exceptions.InvalidSearchCriteriaException;
import org.stories.flightbooking.exceptions.RouteNotFoundException;
import org.stories.flightbooking.model.*;
import org.stories.flightbooking.repository.FlightRepository;
import org.stories.flightbooking.repository.RouteRepository;

import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FlightService
{
    final static Double ECONOMY_70_SLAB = 0.7;
    final static Double ECONOMY_40_SLAB = 0.4;
    final static Double ECONOMY_70_SLAB_PREMIUM = 0.6;
    final static Double ECONOMY_40_SLAB_PREMIUM = 0.3;
    final static double BUSINESS_PREMIUM = 0.4;

    @Autowired
    FlightRepository flightRepository;

    @Autowired
    RouteRepository routeRepository;


    private int calculateDateDiffInDays(LocalDate inputDate)
    {
        LocalDate currentDate = LocalDate.now();
        Period p = Period.between(currentDate, inputDate);
        return  p.getDays();
    }


    public List<FlightSearchResult> searchFlightsForGivenCriteria(SearchCriteria searchCriteria) throws InvalidSearchCriteriaException, RouteNotFoundException, FlightsNotFoundException {

        LocalDate now = LocalDate.now();
        List<FlightSearchResult> flightSearchResults = new ArrayList<>();
        List<Flight> flights = new ArrayList<>();
        if(null == searchCriteria.getSource() || searchCriteria.getSource().isEmpty() || null == searchCriteria.getDestination() || searchCriteria.getDestination().isEmpty())
            throw new InvalidSearchCriteriaException("Source/Destination city is not provided.");
        else
        {
            if(null == searchCriteria.getDepartureDate() || searchCriteria.getDepartureDate().toString().isEmpty())
            {
                searchCriteria.setDepartureDate(now.plusDays(1));
            }
            else if(searchCriteria.getDepartureDate().isBefore(now.plusDays(0)))
            {
                throw new InvalidSearchCriteriaException("Given departure date is in the past.");
            }

            /* Getting routes from routesRepository*/
            List<Route> routes = routeRepository.findAllRoutes();
            Optional<Route> optRoute = routes.stream().filter(
                    r -> (r.getSource().equals(searchCriteria.getSource()) && r.getDestination().equals(searchCriteria.getDestination()))).findFirst();
            optRoute.orElseThrow(() -> new RouteNotFoundException("Route is missing for given Source: " + searchCriteria.getSource() +", Destination: "+ searchCriteria.getDestination()));

            Long routeId = optRoute.get().getRouteId();

            /*Filtering flights based on retrieved routeId*/
            List<Flight> allFlights = flightRepository.findAllFlights();
            flights = allFlights.stream().filter(flt -> (flt.getFk_routeId() == routeId)).collect(Collectors.toList());



            /*Filtering flightclasses matches Travel class*/
            List<Flight> clssWiseFilteredFlights = new ArrayList<>();
            if(null == searchCriteria.getTravelClass() || searchCriteria.getTravelClass().isEmpty())
            {
                searchCriteria.setTravelClass("EC");
            }

            List<FlightClasses> flightClasses =  flightRepository.findAllClasses().stream().filter(flightClass -> flightClass.getClassCode().equals(searchCriteria.getTravelClass())).collect(Collectors.toList());

            /*removing all others flights which are not riding on a given route */
            for(FlightClasses flightClass: flightClasses)
            {
                for(Flight flight : flights)
                {
                    if(flightClass.getFlightId() == flight.getFightId())
                        clssWiseFilteredFlights.add(flight);
                }
            }

            flights = clssWiseFilteredFlights;


            /*Calculating total fares and creating final results*/

            if(flights.size() > 0)
            {
                flightSearchResults = calculateTotalFareForAllSearchedFlights(searchCriteria, flights);
            }else
                throw new FlightsNotFoundException("Flight/Flights not found for the route.");
        }
        return flightSearchResults;
    }

    /* Following logic is well suited in FlightBookingPricingServices, right now there's no finite module so kept here*/
    private List<FlightSearchResult> calculateTotalFareForAllSearchedFlights(SearchCriteria searchCriteria, List<Flight> flights)
    {

        List<FlightSearchResult> results = new ArrayList<>();
        for(Flight flt: flights)
        {
            FlightSearchResult fsr = new FlightSearchResult();
            fsr.setFlightId(flt.getFightId());
            fsr.setFlightName(flt.getFlightName());
            fsr.setTravelClass(searchCriteria.getTravelClass());
            fsr.setNoOfPax(searchCriteria.getNoOfPax());



            Optional<FlightClasses> optFltCls =  flightRepository.findAllClasses().stream().filter
                    (fc -> fc.getClassCode().equals(searchCriteria.getTravelClass()) && fc.getFlightId().equals(flt.getFightId())).findFirst();


            Optional<FlightSeatsStatus> optStatus = flightRepository.findAllFlightSeatsStatus().stream()
                    .filter(st -> st.getClassCode().equals(searchCriteria.getTravelClass()) && st.getFlightId().equals(flt.getFightId())
                            && st.getDepartureDate().equals(searchCriteria.getDepartureDate())).findFirst();



            if(optFltCls.isPresent() && optStatus.isPresent())
            {
                FlightClasses flightClass = optFltCls.get();
                FlightSeatsStatus flightStatus = optStatus.get();
                Double totlFare = calculateFare(flightClass, flightStatus, searchCriteria.getNoOfPax());
                fsr.setTotalFare(totlFare);
            }
            results.add(fsr);
        }
        return  results;

    }

    private Double calculateFare(FlightClasses fc, FlightSeatsStatus fs, Integer noOfPax)
    {
        /* This case can be replaced with sql query*/

        String travelClass = fc.getClassCode();
        Double fare = 0.0;
        double premiumFactor = 0.0;

        switch (travelClass) {
            case "FC":
                double daydiff = 10 - calculateDateDiffInDays(fs.getDepartureDate());
                if (daydiff > -1 && daydiff < 10) {
                    premiumFactor = daydiff * 0.1;
                }
                break;
            case "BC":
                DayOfWeek day = fs.getDepartureDate().getDayOfWeek();
                if (day.equals(DayOfWeek.MONDAY) || day.equals(DayOfWeek.FRIDAY) || day.equals(DayOfWeek.SUNDAY)) {
                    premiumFactor = BUSINESS_PREMIUM;
                }
                break;
            case "EC":
                double filledRatio = fs.getBookedSeats() / fc.getTotalSeats();
                filledRatio = Math.round(filledRatio * 10) / 10.0;
                if (filledRatio > ECONOMY_70_SLAB)
                {
                    premiumFactor = ECONOMY_70_SLAB_PREMIUM;
                } else if (filledRatio > ECONOMY_40_SLAB)
                {
                    premiumFactor = ECONOMY_40_SLAB_PREMIUM;
                }
                break;
        }

        fare = fc.getBasePrice() * (1 + premiumFactor) * noOfPax;
        return fare;

    }
}
