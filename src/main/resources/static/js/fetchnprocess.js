function searchFlight(src, dest, noP, jrnDate, jrnClass)
{
    //$("#srcV").append("");
    //$("#destV").append("");
    var obj = new createCriteria(src, dest, noP, jrnDate, jrnClass);

   // alert(JSON.stringify(obj));
    var reqObj = encodeURI(JSON.stringify(obj));
    var url = "http://localhost:8080/searchFlights?criteria=" + reqObj;
   // alert(url)
    //var value = "Source: " + src;
    //var dval = "Destination: " + dest;

    $("#srcV").append(src);
    $("#destV").append(dest);
    $("#nopx").append(noP);
    $("#jrdate").append(jrnDate);

    $.getJSON(url, function(data){
        processdata(data);
    });
}

function createCriteria(src, dest, noP, jrnDate, jrnClass)
{
    this.source = src;
    this.destination = dest;
    this.noOfPax = noP;
    this.travelClass = jrnClass;
    this.departureDate = jrnDate;
}

function processdata(data)
{
   // alert(data);
    var i = 0;
    var rlength = data.length;
    var markup = [];

    for(i = 0; i < rlength; i++)
    {
       var fare = "";
       if(null == data[i].totalFare)
            fare = "Data not available";
       else
            fare = data[i].totalFare;
       var trmarkup = "<tr><td>"+ data[i].flightId + "</td><td>" + data[i].flightName + "</td><td>"+ data[i].travelClass + "</td><td>" + fare + "</td></tr>";
       markup.push(trmarkup);
      // alert(data[i].flightName)
    }
    $("#resultsTab").append(markup);
}